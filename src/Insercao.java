import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.Connection;
import java.sql.PreparedStatement;


public class Insercao {
    public void inserir( String cpf, String nome, String telefone, String email){
        try {
            Connection c =   Conexao.ObterConexao();
            //PreparedStatement ps=c.prepareStatement("insert into sc_julia_fernandes.cliente(cpf, nome, telefone, email) values(?,?,?,?)");
            PreparedStatement ps=c.prepareStatement("insert into sc_julia_fernades_cliente(cpf, nome, telefone, email) values(?,?,?,?)");
            
            ps.setString(1,cpf);
            ps.setString(2,nome) ;
            ps.setString(3,telefone) ; 
            ps.setString(4,email);
            ps.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
     public void inserirLivro( String codigo,  String titulo, String editora, String autor){
        try {
            Connection c =   Conexao.ObterConexao();
            //PreparedStatement ps=c.prepareStatement("insert into sc_julia_fernandes.cliente(cpf, nome, telefone, email) values(?,?,?,?)");
            PreparedStatement ps=c.prepareStatement("insert into sc_julia_fernades_livro(codigo, titulo , editora, autor) values(?,?,?,?)");
            
            ps.setString(1,codigo);
            ps.setString(2,titulo) ;
            ps.setString(3,editora) ; 
            ps.setString(4,autor);
            ps.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     
     public void inserirFornecedor( int codFornecedor, String nomeFornecedor, String telefone, String cep){
        try {
            Connection c =   Conexao.ObterConexao();
            //PreparedStatement ps=c.prepareStatement("insert into sc_julia_fernandes.cliente(cpf, nome, telefone, email) values(?,?,?,?)");
            PreparedStatement ps=c.prepareStatement("insert into sc_julia_fernades.Fornecedor(codFornecedor, nomeFornecedor, telefone, cep) values(?,?,?,?)");
            
            ps.setInt(1,codFornecedor);
            ps.setString(2,nomeFornecedor) ;
            ps.setString(3,telefone) ; 
            ps.setString(4,cep);
            ps.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
