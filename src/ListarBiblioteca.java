
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JList;

public class ListarBiblioteca {

    public void listar(JList listaCliente) {

        try {
            listaCliente.removeAll();
            DefaultListModel dfm = new DefaultListModel();
            Connection C = Conexao.ObterConexao();
            String SQL = "SELECT * FROM sc_julia_fernades_cliente";
            //PreparedStatement ps = C.prepareStatement(SQL);
            Statement ps = C.createStatement();
            ResultSet rs = ps.executeQuery(SQL);

            while (rs.next()) {
                dfm.addElement(rs.getString("nome"));
            }

            listaCliente.setModel(dfm);

            C.close();
        } catch (SQLException ex) {
            Logger.getLogger(ListarBiblioteca.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void listarLivro(JList listaLivro) {

        try {
            listaLivro.removeAll();
            DefaultListModel dfm = new DefaultListModel();
            Connection C = Conexao.ObterConexao();
            String SQL = "SELECT * FROM sc_julia_fernades_livro";
            PreparedStatement ps = C.prepareStatement(SQL);
            ResultSet rs = ps.executeQuery(SQL);

            while (rs.next()) {
                dfm.addElement(rs.getString("titulo"));
            }

            listaLivro.setModel(dfm);

            C.close();
        } catch (SQLException ex) {
            Logger.getLogger(ListarBiblioteca.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
